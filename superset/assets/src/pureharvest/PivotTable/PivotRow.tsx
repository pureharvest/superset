import * as classNames from 'classnames';
import { isEqual } from 'lodash';
import * as React from 'react';

import CalculatedMetric from 'src/explore/CalculatedMetric';

import { PivotCell, PivotCellProps } from './PivotCell';
import { PivotRowHeaderCell, PivotRowHeaderCellProps } from './PivotRowHeaderCell';
import { ProjectedRow } from './projection';
import { Metric, MetricDisplayOptions } from './transformProps';

interface PivotRowProps {
    columnFormats: { [key: string]: string; };
    displayOptions: { [key: string]: MetricDisplayOptions };
    defaultNumberFormat?: string;
    combineMetrics?: boolean;
    data: ProjectedRow;
    showKey?: boolean;
    depth?: number;
    expanded?: boolean;
    index: string[];
    metrics: Array<Metric | CalculatedMetric>;
    metricPosition?: number;
    onExpand?: (index: string[]) => void;
    onSort?: (rowIndex: string[], columnIndex?: string[], metricName?: string) => void;
    sortColumn?: string[];
    sortMetric?: string;
    sortDirection?: number;
}

function mapHeaderCell(props: PivotRowProps) {
    return (row: ProjectedRow): PivotRowHeaderCellProps[] => {

        const {
            sortDirection = 1,
            sortColumn = [],
            depth = row.index.length - 2,
        } = props;

        const key = row.index[row.index.length - 1];

        const commonProps: PivotRowHeaderCellProps[] = [{
            className: classNames({
                'first-in-group': props.showKey,
                'row-header': true,
            }),
            columnIndex: props.index,
            depth,
            expanded: props.expanded,
            hasChildren: props.showKey && (row.numberOfChildren || 0) > 0,
            onExpand: props.showKey ? props.onExpand : undefined,
            rowIndex: row.index,
            sortDirection: sortColumn.length === 0 ? sortDirection : 0,
            value: props.showKey ? key : '',
        }];

        if (!row.metric) {
            return commonProps;
        }

        const metricProps: PivotRowHeaderCellProps = {
            className: classNames({
                'row-header': true,
            }),
            columnIndex: props.index,
            onExpand: props.onExpand,
            onSort: props.onSort,
            rowIndex: row.index,
            sortDirection: sortColumn.length === 0 ? sortDirection : 0,
            value: row.metric,
        };

        return commonProps.concat(metricProps);
    };
}

function mapValueCells(props: PivotRowProps, row: ProjectedRow) {
    return (value: { index: string[], metric: string, value: number }): PivotCellProps => {
        const {
            expanded = false,
            sortDirection = 1,
            sortColumn = [],
            sortMetric = '',
            defaultNumberFormat = '',
            displayOptions = {},
            columnFormats = {},
        } = props;
        const hasChildren = row.numberOfChildren !== undefined ? row.numberOfChildren > 0 : false;

        return {
            colorCode: displayOptions[value.metric].colorCodeValues,
            columnIndex: value.index,
            expanded,
            format: displayOptions[value.metric].format || columnFormats[value.metric] || defaultNumberFormat,
            hasChildren,
            metricName: value.metric,
            onSort: props.onSort,
            rowIndex: row.index,
            sortDirection: isEqual(sortColumn, value.index) && sortMetric === value.metric ? sortDirection : 0,
            value: value.value,
        };

    };
}

export const PivotRow = React.memo((props: PivotRowProps) => {
    const { data } = props;

    const className = classNames({
        'expanded': props.expanded,
        'has-children': (data.numberOfChildren || 0) > 0,
    });
    const key = data.index.join('-');
    const header = mapHeaderCell(props)(data);
    const cellProps = data.data.map(mapValueCells(props, data));
    const headerCells = header.map((cellProp, i) => (<PivotRowHeaderCell key={`th-${key}-${i}`} {...cellProp} />));
    const cells = cellProps.map(
        (cellProp, i) => (
            <PivotCell
                key={`${key}-${i}`}
                {...cellProp}
            />
        ),
    );
    return (
        <tr className={className} >
            {headerCells}
            {cells}
        </tr>
    );

});
