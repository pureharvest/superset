
export const MetricPosition = {
  Rows: 0,
  RowsCombined: 1,
  Columns: 2,
  ColumnsCombined: 3,
};
