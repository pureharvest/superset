/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import { unionBy, intersectionBy, isEqual } from 'lodash';
import React from 'react';

import { ListGroup, ListGroupItem } from 'react-bootstrap';
import {
  SortableContainer, SortableHandle, SortableElement, arrayMove,
} from 'react-sortable-hoc';

import ControlHeader from '../ControlHeader';
import MetricDisplayOption from '../MetricDisplayOption';

const defaultProps = {
  label: null,
  description: null,
  onChange: () => { },
  placeholder: 'Empty collection',
  keyAccessor: o => o.name,
  metrics: [],
  value: [],
};

const SortableListGroupItem = SortableElement(ListGroupItem);
const SortableListGroup = SortableContainer(ListGroup);
const SortableDragger = SortableHandle(() => (
  <i className="fa fa-bars text-primary" style={{ cursor: 'ns-resize' }} />));

export default class MetricsDisplayOrderControl extends React.Component {

  componentWillReceiveProps(nextProps) {
    const keepMetrics = intersectionBy(this.props.value, nextProps.metrics, 'name');
    const calculatedValue = unionBy(keepMetrics, nextProps.metrics, 'name');

    if (!isEqual(calculatedValue, this.props.value)) {
      this.props.onChange(calculatedValue.slice());
    }
  }

  onChange(i, value) {
    Object.assign(this.props.value[i], value);
    this.props.onChange(this.props.value);
  }

  onSortEnd({ oldIndex, newIndex }) {
    this.props.onChange(arrayMove(this.props.value, oldIndex, newIndex));
  }

  renderList() {
    if (this.props.value.length === 0) {
      return <div className="text-muted">{this.props.placeholder}</div>;
    }

    return (
      <SortableListGroup
        useDragHandle
        lockAxis="y"
        onSortEnd={this.onSortEnd.bind(this)}
      >
        {this.props.value.map((o, i) => (
          <SortableListGroupItem
            className="clearfix"
            key={this.props.keyAccessor(o)}
            index={i}
          >
            <div className="pull-left m-r-5">
              <SortableDragger />
            </div>
            <div className="pull-left">
              <MetricDisplayOption {...o} onChange={this.onChange.bind(this, i)} />
            </div>
          </SortableListGroupItem>))}
      </SortableListGroup>
    );
  }

  render() {
    return (
      <div className="MetricsDisplayOrderControl">
        <ControlHeader {...this.props} />
        {this.renderList()}
      </div>
    );
  }
}

MetricsDisplayOrderControl.defaultProps = defaultProps;
