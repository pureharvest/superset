/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import { isEqual } from 'lodash';

import {
  AggregatePosition, NestedStates, nestedWithAggregatesReducer, sortNested, updateNestedState,
} from '../../../../src/pureharvest/PivotTable/nesting';
import {
  getValueForSort, projectMetricsAsColumns, projectMetricsAsRows,
} from '../../../../src/pureharvest/PivotTable/projection';

import {
  Row, transformData,
} from '../../../../src/pureharvest/PivotTable/transformProps';
import '../../../helpers/shim';

describe('pivot table projections', () => {

  describe('project metrics in columns', () => {
    const data = {
      columns: ['a', 'b'],
      metrics: [{
        d3format: null,
        description: null,
        expression: 'SUM(m1)',
        id: 1,
        metric_name: 'sum_m1',
        verbose_name: null,
        waring_text: null,
      },
      {
        d3format: null,
        description: null,
        expression: 'SUM(m2)',
        id: 2,
        metric_name: 'sum_m2',
        verbose_name: null,
        waring_text: null,
      }],
      tableData: {
        columns: [
          ['sum_m1', 'a1', 'b1'],
          ['sum_m1', 'a1', 'b2'],
          ['sum_m1', 'a2', 'b1'],
          ['sum_m1', 'a2', 'b2'],
          ['sum_m2', 'a1', 'b1'],
          ['sum_m2', 'a1', 'b2'],
          ['sum_m2', 'a2', 'b1'],
          ['sum_m2', 'a2', 'b2'],
        ],
        data: [
          [1, 2, 3, 4, 1, 2, 3, 4],
          [5, 6, 7, 8, 5, 6, 7, 8],
          [9, 10, 11, 12, 9, 10, 11, 12],
          [13, 14, 15, 16, 13, 14, 15, 16],
        ],
        index: ['r1', 'r2', 'r3', 'r4'],
      },
    };

    const transformedData = transformData(data.tableData, data.columns, data.metrics, []);

    it('handles hidden aggregates', () => {
      const projectionFn = projectMetricsAsColumns(data.metrics, true, AggregatePosition.Hidden);
      const reducer = nestedWithAggregatesReducer(projectionFn)([]);
      const rows = transformedData.rows.reduce(reducer, []);

      expect(rows).toEqual([
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 1 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 1 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 2 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 2 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 4 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 4 },
          ],
          index: ['r1'],
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 5 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 5 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 6 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 6 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 8 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 8 },
          ],
          index: ['r2'],
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 9 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 9 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 10 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 10 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 12 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 12 },
          ],
          index: ['r3'],
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 13 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 13 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 14 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 14 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 16 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 16 },
          ],
          index: ['r4'],
          numberOfChildren: 0,
        },
      ]);

    });

    it('handles after aggregates', () => {
      const projectionFn = projectMetricsAsColumns(data.metrics, true, AggregatePosition.After);
      const reducer = nestedWithAggregatesReducer(projectionFn)([]);
      const rows = transformedData.rows.reduce(reducer, []);

      expect(rows).toEqual([
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 1 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 1 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 2 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 2 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 3 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 4 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 4 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 7 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 7 },
          ],
          index: ['r1'],
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 5 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 5 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 6 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 6 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 11 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 8 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 8 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 15 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 15 },
          ],
          index: ['r2'],
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 9 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 9 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 10 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 10 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 19 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 19 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 12 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 12 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 23 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 23 },
          ],
          index: ['r3'],
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 13 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 13 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 14 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 14 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 27 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 27 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 16 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 16 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 31 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 31 },
          ],
          index: ['r4'],
          numberOfChildren: 0,
        },
      ]);

    });
  });

  describe('project metrics in rows', () => {

    const data = {
      columns: ['a', 'b'],
      metrics: [{
        d3format: null,
        description: null,
        expression: 'SUM(m1)',
        id: 1,
        metric_name: 'sum_m1',
        verbose_name: null,
        waring_text: null,
      },
      {
        d3format: null,
        description: null,
        expression: 'SUM(m2)',
        id: 2,
        metric_name: 'sum_m2',
        verbose_name: null,
        waring_text: null,
      }],
      tableData: {
        columns: [
          ['sum_m1', 'a1', 'b1'],
          ['sum_m1', 'a1', 'b2'],
          ['sum_m1', 'a2', 'b1'],
          ['sum_m1', 'a2', 'b2'],
          ['sum_m2', 'a1', 'b1'],
          ['sum_m2', 'a1', 'b2'],
          ['sum_m2', 'a2', 'b1'],
          ['sum_m2', 'a2', 'b2'],
        ],
        data: [
          [1, 2, 3, 4, 1, 2, 3, 4],
          [5, 6, 7, 8, 5, 6, 7, 8],
          [9, 10, 11, 12, 9, 10, 11, 12],
          [13, 14, 15, 16, 13, 14, 15, 16],
        ],
        index: ['r1', 'r2', 'r3', 'r4'],
      },
    };

    const transformedData = transformData(data.tableData, data.columns, data.metrics, []);

    it('handles after aggregates', () => {
      const projectionFn = projectMetricsAsRows(data.metrics, true, AggregatePosition.After);
      const reducer = nestedWithAggregatesReducer(projectionFn)([]);
      const rows = transformedData.rows.reduce(reducer, []);

      expect(rows).toEqual([
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 1 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 2 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 4 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 7 },
          ],
          index: ['r1'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 1 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 2 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 4 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 7 },
          ],
          index: ['r1'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 5 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 6 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 8 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 15 },
          ],
          index: ['r2'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 5 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 6 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 8 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 15 },
          ],
          index: ['r2'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 9 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 10 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 19 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 12 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 23 },
          ],
          index: ['r3'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 9 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 10 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 19 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 12 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 23 },
          ],
          index: ['r3'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 13 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 14 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 27 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 16 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 31 },
          ],
          index: ['r4'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 13 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 14 },
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 27 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 16 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 31 },
          ],
          index: ['r4'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
      ]);
    });

    it('handles hidden aggregates', () => {
      const projectionFn = projectMetricsAsRows(data.metrics, true, AggregatePosition.Hidden);
      const reducer = nestedWithAggregatesReducer(projectionFn)([]);
      const rows = transformedData.rows.reduce(reducer, []);

      expect(rows).toEqual([
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 1 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 2 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 4 },
          ],
          index: ['r1'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 1 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 2 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 4 },
          ],
          index: ['r1'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 5 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 6 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 8 },
          ],
          index: ['r2'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 5 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 6 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 8 },
          ],
          index: ['r2'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 9 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 10 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 12 },
          ],
          index: ['r3'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 9 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 10 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 12 },
          ],
          index: ['r3'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 13 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 14 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 16 },
          ],
          index: ['r4'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 13 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 14 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 16 },
          ],
          index: ['r4'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
      ]);

    });

    it('handles before aggregates', () => {
      const projectionFn = projectMetricsAsRows(data.metrics, true, AggregatePosition.Before);
      const reducer = nestedWithAggregatesReducer(projectionFn)([]);
      const rows = transformedData.rows.reduce(reducer, []);

      expect(rows).toEqual([
        {
          data: [
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 3 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 1 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 2 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 4 },
          ],
          index: ['r1'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 3 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 1 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 2 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 3 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 4 },
          ],
          index: ['r1'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 11 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 5 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 6 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 8 },
          ],
          index: ['r2'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 11 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 5 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 6 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 7 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 8 },
          ],
          index: ['r2'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 19 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 9 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 10 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 23 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 12 },
          ],
          index: ['r3'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 19 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 9 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 10 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 23 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 11 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 12 },
          ],
          index: ['r3'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m1', value: 27 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m1', value: 13 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m1', value: 14 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m1', value: 31 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m1', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m1', value: 16 },
          ],
          index: ['r4'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [
            { numberOfChildren: 2, index: ['a1'], metric: 'sum_m2', value: 27 },
            { numberOfChildren: 0, index: ['a1', 'b1'], metric: 'sum_m2', value: 13 },
            { numberOfChildren: 0, index: ['a1', 'b2'], metric: 'sum_m2', value: 14 },
            { numberOfChildren: 2, index: ['a2'], metric: 'sum_m2', value: 31 },
            { numberOfChildren: 0, index: ['a2', 'b1'], metric: 'sum_m2', value: 15 },
            { numberOfChildren: 0, index: ['a2', 'b2'], metric: 'sum_m2', value: 16 },
          ],
          index: ['r4'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
      ]);

    });
  });

  describe('project metrics in rows with no columns', () => {

    const data = {
      columns: [],
      metrics: [{
        d3format: null,
        description: null,
        expression: 'SUM(m1)',
        id: 1,
        metric_name: 'sum_m1',
        verbose_name: null,
        waring_text: null,
      },
      {
        d3format: null,
        description: null,
        expression: 'SUM(m2)',
        id: 2,
        metric_name: 'sum_m2',
        verbose_name: null,
        waring_text: null,
      }],
      tableData: {
        columns: [
          ['sum_m1'],
          ['sum_m2'],
        ],
        data: [
          [1, 2],
          [5, 6],
          [9, 10],
          [13, 14],
        ],
        index: ['r1', 'r2', 'r3', 'r4'],
      },
    };
    const transformedData = transformData(data.tableData, data.columns, data.metrics, []);
    it('handles after aggregates', () => {
      const projectionFn = projectMetricsAsRows(data.metrics, true, AggregatePosition.After);
      const reducer = nestedWithAggregatesReducer(projectionFn)([]);
      const rows = transformedData.rows.reduce(reducer, []);

      expect(rows).toEqual([
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 1 }],
          index: ['r1'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 2 }],
          index: ['r1'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 5 }],
          index: ['r2'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 6 }],
          index: ['r2'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 9 }],
          index: ['r3'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 10 }],
          index: ['r3'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 13 }],
          index: ['r4'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 14 }],
          index: ['r4'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
      ]);
    });
  });

  describe('project metrics in rows with multiple groups', () => {

    const data = {
      columns: [],
      metrics: [{
        d3format: null,
        description: null,
        expression: 'SUM(m1)',
        id: 1,
        metric_name: 'sum_m1',
        verbose_name: null,
        waring_text: null,
      },
      {
        d3format: null,
        description: null,
        expression: 'SUM(m2)',
        id: 2,
        metric_name: 'sum_m2',
        verbose_name: null,
        waring_text: null,
      }],
      tableData: {
        columns: [
          ['sum_m1'],
          ['sum_m2'],
        ],
        data: [
          [1, 2],
          [3, 4],
          [5, 6],
          [7, 8],
        ],
        index: [
          ['r1', 'rr1'],
          ['r1', 'rr2'],
          ['r2', 'rr1'],
          ['r2', 'rr2'],
        ],
      },
    };
    const transformedData = transformData(data.tableData, data.columns, data.metrics, []);
    it('handles hidden aggregates', () => {
      const projectionFn = projectMetricsAsRows(data.metrics, true, AggregatePosition.Hidden);
      const reducer = nestedWithAggregatesReducer(projectionFn)([]);
      const rows = transformedData.rows.reduce(reducer, []);

      expect(rows).toEqual([
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 1 }],
          index: ['r1', 'rr1'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 2 }],
          index: ['r1', 'rr1'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 3 }],
          index: ['r1', 'rr2'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 4 }],
          index: ['r1', 'rr2'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 5 }],
          index: ['r2', 'rr1'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 6 }],
          index: ['r2', 'rr1'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 7 }],
          index: ['r2', 'rr2'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 8 }],
          index: ['r2', 'rr2'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
      ]);
    });

    it('handles after aggregates', () => {
      const projectionFn = projectMetricsAsRows(data.metrics, true, AggregatePosition.After);
      const reducer = nestedWithAggregatesReducer(projectionFn, AggregatePosition.After)([]);
      const rows = transformedData.rows.reduce(reducer, []);

      expect(rows).toEqual([
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 1 }],
          index: ['r1', 'rr1'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 2 }],
          index: ['r1', 'rr1'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 3 }],
          index: ['r1', 'rr2'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 4 }],
          index: ['r1', 'rr2'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 4 }],
          index: ['r1'],
          metric: 'sum_m1',
          numberOfChildren: 2,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 6 }],
          index: ['r1'],
          metric: 'sum_m2',
          numberOfChildren: 2,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 5 }],
          index: ['r2', 'rr1'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 6 }],
          index: ['r2', 'rr1'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 7 }],
          index: ['r2', 'rr2'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 8 }],
          index: ['r2', 'rr2'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 12 }],
          index: ['r2'],
          metric: 'sum_m1',
          numberOfChildren: 2,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 14 }],
          index: ['r2'],
          metric: 'sum_m2',
          numberOfChildren: 2,
        },
      ]);
    });

    it('handles functional aggregate position', () => {
      const aggregatePosition = (index: string[]) => {
        if (isEqual(index, ['r1'])) {
          return AggregatePosition.Before;
        }

        return AggregatePosition.Replace;
      };

      const projectionFn = projectMetricsAsRows(data.metrics, true, AggregatePosition.After);
      const reducer = nestedWithAggregatesReducer(projectionFn, aggregatePosition)([]);
      const rows = transformedData.rows.reduce(reducer, []);

      expect(rows).toEqual([
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 4 }],
          index: ['r1'],
          metric: 'sum_m1',
          numberOfChildren: 2,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 6 }],
          index: ['r1'],
          metric: 'sum_m2',
          numberOfChildren: 2,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 1 }],
          index: ['r1', 'rr1'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 2 }],
          index: ['r1', 'rr1'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 3 }],
          index: ['r1', 'rr2'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 4 }],
          index: ['r1', 'rr2'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 12 }],
          index: ['r2'],
          metric: 'sum_m1',
          numberOfChildren: 2,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 14 }],
          index: ['r2'],
          metric: 'sum_m2',
          numberOfChildren: 2,
        },
      ]);
    });

    it('handles sorting', () => {
      const getSortFn = (index: string[]) => {
        if (isEqual(index, ['r1'])) {
          return (a: Row, b: Row) => {
            const aKey = getValueForSort([''], 'sum_m1')(a);
            const bKey = getValueForSort([''], 'sum_m1')(a);
            return aKey - bKey * -1;
          };
        }

        if (isEqual(index, ['r2'])) {
          return (a: Row, b: Row) => {
            const aKey = getValueForSort([''], 'sum_m2')(a);
            const bKey = getValueForSort([''], 'sum_m2')(a);
            return aKey - bKey;
          };
        }

        return () => {
          return 0;
        };
      };

      const sortedRows = sortNested(getSortFn)(transformedData.rows, []);
      const projectionFn = projectMetricsAsRows(data.metrics, true, AggregatePosition.After);
      const reducer = nestedWithAggregatesReducer(projectionFn, AggregatePosition.Before)([]);
      const rows = sortedRows.reduce(reducer, []);

      expect(rows).toEqual([
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 4 }],
          index: ['r1'],
          metric: 'sum_m1',
          numberOfChildren: 2,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 6 }],
          index: ['r1'],
          metric: 'sum_m2',
          numberOfChildren: 2,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 3 }],
          index: ['r1', 'rr2'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 4 }],
          index: ['r1', 'rr2'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 1 }],
          index: ['r1', 'rr1'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 2 }],
          index: ['r1', 'rr1'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 12 }],
          index: ['r2'],
          metric: 'sum_m1',
          numberOfChildren: 2,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 14 }],
          index: ['r2'],
          metric: 'sum_m2',
          numberOfChildren: 2,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 5 }],
          index: ['r2', 'rr1'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 6 }],
          index: ['r2', 'rr1'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m1', value: 7 }],
          index: ['r2', 'rr2'],
          metric: 'sum_m1',
          numberOfChildren: 0,
        },
        {
          data: [{ numberOfChildren: 0, index: [''], metric: 'sum_m2', value: 8 }],
          index: ['r2', 'rr2'],
          metric: 'sum_m2',
          numberOfChildren: 0,
        },
      ]);
    });
  });

  describe('nested states', () => {

    it('can be updated', () => {
      const states: NestedStates<boolean> = {};

      const newStates = updateNestedState(() => false)(states, ['a']);

      expect(newStates).toEqual({
        a: {
          key: 'a',
          state: false,
        },
      });
    });

    it('can update nested', () => {
      const states: NestedStates<boolean> = {};

      const newStates = updateNestedState(() => true)(states, ['a', 'b']);

      expect(newStates).toEqual({
        a: {
          children: {
            b: {
              key: 'b',
              state: true,
            },
          },
          key: 'a',
        },
      });
    });

    it('sets default state', () => {
      const states: NestedStates<boolean> = {};

      const newStates = updateNestedState(() => true, false)(states, ['a', 'b']);

      expect(newStates).toEqual({
        a: {
          children: {
            b: {
              key: 'b',
              state: true,
            },
          },
          key: 'a',
          state: false,
        },
      });
    });
  });
});
