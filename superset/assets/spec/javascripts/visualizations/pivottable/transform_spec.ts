/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import {
  aggregateNested,
  Nested,
  NestedWithAggregates,
} from '../../../../src/pureharvest/PivotTable/nesting';

import {
  aggregateColumns,
  aggregateRows,
  createColumnNamesNester,
  createColumnNester,
  createRowNester,
  Row,
} from '../../../../src/pureharvest/PivotTable/transformProps';

import '../../../helpers/shim';

describe('pivot table', () => {
  describe('nesting column headers', () => {
    it('handles no columns and multiple metrics', () => {
      const columns: string[] = [];
      const columnIndexes = [
        ['m1'],
        ['m2'],
      ];

      const nester = createColumnNamesNester(columns);
      const nestedColumns = nester(columnIndexes);
      const aggregatedNestedColumnes = nestedColumns.map(aggregateNested<string[]>((values, index) => {
        return index || [];
      }));

      expect(aggregatedNestedColumnes).toEqual([]);
    });

    it('handles single column and multiple metrics', () => {
      const columns: string[] = ['a'];
      const columnIndexes = [
        ['m1', 'a1'],
        ['m1', 'a2'],
        ['m2', 'a1'],
        ['m2', 'a2'],
      ];

      const nester = createColumnNamesNester(columns);
      const nestedColumns = nester(columnIndexes);
      const aggregatedNestedColumnes = nestedColumns.map(aggregateNested<string[]>((values, index) => {
        return index || [];
      }));

      expect(aggregatedNestedColumnes).toEqual([
        {
          aggregated: ['a1'],
          key: 'a1',
          values: [['a1']],
        },
        {
          aggregated: ['a2'],
          key: 'a2',
          values: [['a2']],
        },
      ]);
    });

    it('handles multiple columns and multiple metrics', () => {
      const columns: string[] = ['a', 'b'];
      const columnIndexes = [
        ['m1', 'a1', 'b1'],
        ['m1', 'a1', 'b2'],
        ['m1', 'a2', 'b1'],
        ['m1', 'a2', 'b2'],
        ['m2', 'a1', 'b1'],
        ['m2', 'a1', 'b2'],
        ['m2', 'a2', 'b1'],
        ['m2', 'a2', 'b2'],
      ];

      const nester = createColumnNamesNester(columns);
      const nestedColumns = nester(columnIndexes);
      const aggregatedNestedColumnes = nestedColumns.map(aggregateNested<string[]>((values, index) => {
        return index || [];
      }));

      expect(aggregatedNestedColumnes).toEqual([
        {
          aggregated: ['a1'],
          key: 'a1',
          values: [
            { aggregated: ['a1', 'b1'], key: 'b1', values: [['a1', 'b1']] },
            { aggregated: ['a1', 'b2'], key: 'b2', values: [['a1', 'b2']] },
          ],
        },
        {
          aggregated: ['a2'],
          key: 'a2',
          values: [
            { aggregated: ['a2', 'b1'], key: 'b1', values: [['a2', 'b1']] },
            { aggregated: ['a2', 'b2'], key: 'b2', values: [['a2', 'b2']] },
          ],
        },
      ]);
    });
  });

  // describe('projecting column headers', () => {
  //   const props = {
  //     columnNames: ['a', 'b'],
  //     metrics: [{
  //       d3format: null,
  //       description: null,
  //       expression: 'SUM(m1)',
  //       id: 1,
  //       metric_name: 'sum_m1',
  //       verbose_name: null,
  //       waring_text: null,
  //     }],
  //     nestedColumns: [
  //       {
  //         aggregated: ['a1'],
  //         key: 'a1',
  //         values: [
  //           { aggregated: ['a1', 'b1'], key: 'b1', values: [['a1', 'b1']] },
  //           { aggregated: ['a1', 'b2'], key: 'b2', values: [['a1', 'b2']] },
  //         ],
  //       },
  //       {
  //         aggregated: ['a2'],
  //         key: 'a2',
  //         values: [
  //           { aggregated: ['a2', 'b1'], key: 'b1', values: [['a2', 'b1']] },
  //           { aggregated: ['a2', 'b2'], key: 'b2', values: [['a2', 'b2']] },
  //         ],
  //       },
  //     ],
  //   };

  //   // it('handles combined metrics and totals', () => {
  //   //   const headers = projectColumnHeaders(props.nestedColumns, props.columnNames, props.metrics);

  //   //   expect(headers).toEqual([
  //   //     ['a1', 'a1', 'a1', 'a2', 'a2', 'a2'],
  //   //     ['b1', 'b2', 'All', 'b1', 'b2', 'All'],
  //   //     ['sum_m1', 'sum_m1', 'sum_m1', 'sum_m1', 'sum_m1', 'sum_m1'],
  //   //   ]);
  //   // });

  //   // it('handles combined metrics and totals before', () => {
  //   //   const headers = projectColumnHeaders(
  //   //     props.nestedColumns,
  //   //     props.columnNames,
  //   //     props.metrics,
  //   //     true,
  //   //     MetricsPosition.Columns,
  //   //     AggregatePosition.Before);

  //   //   expect(headers).toEqual([
  //   //     ['a1', 'a1', 'a1', 'a2', 'a2', 'a2'],
  //   //     ['All', 'b1', 'b2', 'All', 'b1', 'b2'],
  //   //     ['sum_m1', 'sum_m1', 'sum_m1', 'sum_m1', 'sum_m1', 'sum_m1'],
  //   //   ]);
  //   // });

  //   // it('handles non combined metrics and totals before', () => {
  //   //   const headers = projectColumnHeaders(
  //   //     props.nestedColumns,
  //   //     props.columnNames,
  //   //     props.metrics,
  //   //     false,
  //   //     MetricsPosition.Columns,
  //   //     AggregatePosition.Before);

  //   //   expect(headers).toEqual([
  //   //     ['sum_m1', 'sum_m1', 'sum_m1', 'sum_m1', 'sum_m1', 'sum_m1'],
  //   //     ['a1', 'a1', 'a1', 'a2', 'a2', 'a2'],
  //   //     ['All', 'b1', 'b2', 'All', 'b1', 'b2'],
  //   //   ]);
  //   // });

  //   // it('handles metrics on rows', () => {
  //   //   const headers = projectColumnHeaders(
  //   //     props.nestedColumns,
  //   //     props.columnNames,
  //   //     props.metrics,
  //   //     false,
  //   //     MetricsPosition.Rows,
  //   //     AggregatePosition.Before);

  //   //   expect(headers).toEqual([
  //   //     ['a1', 'a1', 'a1', 'a2', 'a2', 'a2'],
  //   //     ['All', 'b1', 'b2', 'All', 'b1', 'b2'],
  //   //   ]);
  //   // });

  // });

  describe('column mapping', () => {
    it('handles single column and single metrics', () => {
      const metrics = [{
        d3format: null,
        description: null,
        expression: 'SUM(m1)',
        id: 1,
        metric_name: 'm1',
        verbose_name: null,
        waring_text: null,
      }];
      const columns = ['c'];
      const columnIndexes = [['m1', 'c1'], ['m1', 'c2'], ['m1', 'c3']];

      const columnMapper = createColumnNester(metrics, [])(columns, columnIndexes);

      const mapped = columnMapper([1, 2, 3]);

      expect(mapped).toEqual([
        { key: 'c1', values: [{ index: ['c1'], metrics: { m1: 1 } }] },
        { key: 'c2', values: [{ index: ['c2'], metrics: { m1: 2 } }] },
        { key: 'c3', values: [{ index: ['c3'], metrics: { m1: 3 } }] },
      ]);
    });

    it('handles no columns and multiple metrics', () => {
      const metrics = [{
        d3format: null,
        description: null,
        expression: 'SUM(m1)',
        id: 1,
        metric_name: 'm1',
        verbose_name: null,
        waring_text: null,
      },
      {
        d3format: null,
        description: null,
        expression: 'SUM(m2)',
        id: 1,
        metric_name: 'm2',
        verbose_name: null,
        waring_text: null,
      }];
      const columns: string[] = [];
      const columnIndexes = [['m1'], ['m2']];

      const columnMapper = createColumnNester(metrics, [])(columns, columnIndexes);

      const mapped = columnMapper([1, 2]);

      expect(mapped).toEqual([
        { key: '', values: [{ index: [''], metrics: { m1: 1, m2: 2 } }] },
      ]);
    });

    it('handles multiple columns and multiple metrics', () => {
      const metrics = [{
        d3format: null,
        description: null,
        expression: 'SUM(m1)',
        id: 1,
        metric_name: 'm1',
        verbose_name: null,
        waring_text: null,
      },
      {
        d3format: null,
        description: null,
        expression: 'SUM(m2)',
        id: 1,
        metric_name: 'm2',
        verbose_name: null,
        waring_text: null,
      }];
      const columns: string[] = ['a', 'b'];
      const columnIndexes = [
        ['m1', 'a1', 'b1'],
        ['m1', 'a1', 'b2'],
        ['m1', 'a2', 'b1'],
        ['m1', 'a2', 'b2'],
        ['m2', 'a1', 'b1'],
        ['m2', 'a1', 'b2'],
        ['m2', 'a2', 'b1'],
        ['m2', 'a2', 'b2'],
      ];

      const columnMapper = createColumnNester(metrics, [])(columns, columnIndexes);

      const mapped = columnMapper([1, 2, 3, 4, 5, 6, 7, 8]);

      expect(mapped).toEqual([
        {
          key: 'a1',
          values: [{
            key: 'b1',
            values: [{ index: ['a1', 'b1'], metrics: { m1: 1, m2: 5 } }],
          }, {
            key: 'b2',
            values: [{ index: ['a1', 'b2'], metrics: { m1: 2, m2: 6 } }],
          }],
        },
        {
          key: 'a2',
          values: [{
            key: 'b1',
            values: [{ index: ['a2', 'b1'], metrics: { m1: 3, m2: 7 } }],
          }, {
            key: 'b2',
            values: [{ index: ['a2', 'b2'], metrics: { m1: 4, m2: 8 } }],
          }],
        },
      ]);
    });

  });

  describe('column aggregating', () => {
    const metrics = [{
      d3format: null,
      description: null,
      expression: 'SUM(m1)',
      id: 1,
      metric_name: 'sum_m1',
      verbose_name: null,
      waring_text: null,
    }, {
      d3format: null,
      description: null,
      expression: 'SUM(m2)',
      id: 1,
      metric_name: 'sum_m2',
      verbose_name: null,
      waring_text: null,
    }];

    const sourceColumns = [
      {
        key: 'a1',
        values: [{
          key: 'b1',
          values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 1, sum_m2: 5 } }],
        }, {
          key: 'b2',
          values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 2, sum_m2: 6 } }],
        }],
      },
      {
        key: 'a2',
        values: [{
          key: 'b1',
          values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 3, sum_m2: 7 } }],
        }, {
          key: 'b2',
          values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 4, sum_m2: 8 } }],
        }],
      },
    ];

    it('sums columns', () => {

      const aggFn = aggregateColumns(metrics, []);

      const aggregated = sourceColumns.map(aggregateNested(aggFn));

      expect(aggregated).toEqual([
        {
          aggregated: { index: ['a1'], metrics: { sum_m1: 3, sum_m2: 11 } },
          key: 'a1',
          values: [{
            aggregated: { index: ['a1', 'b1'], metrics: { sum_m1: 1, sum_m2: 5 } },
            key: 'b1',
            values: [{ index: ['a1', 'b1'], metrics: { sum_m1: 1, sum_m2: 5 } }],
          }, {
            aggregated: { index: ['a1', 'b2'], metrics: { sum_m1: 2, sum_m2: 6 } },
            key: 'b2',
            values: [{ index: ['a1', 'b2'], metrics: { sum_m1: 2, sum_m2: 6 } }],
          }],
        },
        {
          aggregated: { index: ['a2'], metrics: { sum_m1: 7, sum_m2: 15 } },
          key: 'a2',
          values: [{
            aggregated: { index: ['a2', 'b1'], metrics: { sum_m1: 3, sum_m2: 7 } },
            key: 'b1',
            values: [{ index: ['a2', 'b1'], metrics: { sum_m1: 3, sum_m2: 7 } }],
          }, {
            aggregated: { index: ['a2', 'b2'], metrics: { sum_m1: 4, sum_m2: 8 } },
            key: 'b2',
            values: [{ index: ['a2', 'b2'], metrics: { sum_m1: 4, sum_m2: 8 } }],
          }],
        },
      ]);
    });
  });

  describe('row nesting', () => {
    const sourceRows: Row[] = [
      {
        columns: [
          {
            aggregated: { index: [''], metrics: { sum_m1: 1, sum_m2: 2 } },
            key: '',
            values: [{ index: [''], metrics: { sum_m1: 1, sum_m2: 2 } }],
          },
        ],
        index: ['a1', 'b1'],
      },
      {
        columns: [
          {
            aggregated: { index: [''], metrics: { sum_m1: 3, sum_m2: 4 } },
            key: '',
            values: [{ index: [''], metrics: { sum_m1: 3, sum_m2: 4 } }],
          },
        ],
        index: ['a1', 'b2'],
      },
      {
        columns: [
          {
            aggregated: { index: [''], metrics: { sum_m1: 5, sum_m2: 6 } },
            key: '',
            values: [{ index: [''], metrics: { sum_m1: 5, sum_m2: 6 } }],
          },
        ],
        index: ['a2', 'b1'],
      },
      {
        columns: [
          {
            aggregated: { index: [''], metrics: { sum_m1: 5, sum_m2: 6 } },
            key: '',
            values: [{ index: [''], metrics: { sum_m1: 5, sum_m2: 6 } }],
          },
        ],
        index: ['a2', 'b2'],
      },
    ];

    const nestedRows: Array<Nested<Row>> = [
      {
        key: 'a1',
        values: [
          {
            key: 'b1',
            values: [{
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 1, sum_m2: 2 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 1, sum_m2: 2 } }],
                },
              ],
              index: ['a1', 'b1'],
            }],
          },
          {
            key: 'b2',
            values: [{
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 3, sum_m2: 4 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 3, sum_m2: 4 } }],
                },
              ],
              index: ['a1', 'b2'],
            }],
          }],
      }, {
        key: 'a2',
        values: [
          {
            key: 'b1',
            values: [{
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 5, sum_m2: 6 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 5, sum_m2: 6 } }],
                },
              ],
              index: ['a2', 'b1'],
            }],
          }, {
            key: 'b2',
            values: [{
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 5, sum_m2: 6 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 5, sum_m2: 6 } }],
                },
              ],
              index: ['a2', 'b2'],
            }],
          }],
      },
    ];

    const aggregatedRows: Array<NestedWithAggregates<Row>> = [
      {
        aggregated: {
          columns: [
            {
              aggregated: { index: [''], metrics: { sum_m1: 4, sum_m2: 6 } },
              key: '',
              values: [{ index: [''], metrics: { sum_m1: 4, sum_m2: 6 } }],
            },
          ],
          index: ['a1'],
        },
        key: 'a1',
        values: [
          {
            aggregated: {
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 1, sum_m2: 2 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 1, sum_m2: 2 } }],
                },
              ],
              index: ['a1', 'b1'],
            },
            key: 'b1',
            values: [{
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 1, sum_m2: 2 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 1, sum_m2: 2 } }],
                },
              ],
              index: ['a1', 'b1'],
            }],
          },
          {
            aggregated: {
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 3, sum_m2: 4 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 3, sum_m2: 4 } }],
                },
              ],
              index: ['a1', 'b2'],
            },
            key: 'b2',
            values: [{
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 3, sum_m2: 4 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 3, sum_m2: 4 } }],
                },
              ],
              index: ['a1', 'b2'],
            }],
          }],
      }, {
        aggregated: {
          columns: [
            {
              aggregated: { index: [''], metrics: { sum_m1: 10, sum_m2: 12 } },
              key: '',
              values: [{ index: [''], metrics: { sum_m1: 10, sum_m2: 12 } }],
            },
          ],
          index: ['a2'],
        },
        key: 'a2',
        values: [
          {
            aggregated: {
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 5, sum_m2: 6 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 5, sum_m2: 6 } }],
                },
              ],
              index: ['a2', 'b1'],
            },
            key: 'b1',
            values: [{
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 5, sum_m2: 6 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 5, sum_m2: 6 } }],
                },
              ],
              index: ['a2', 'b1'],
            }],
          }, {
            aggregated: {
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 5, sum_m2: 6 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 5, sum_m2: 6 } }],
                },
              ],
              index: ['a2', 'b2'],
            },
            key: 'b2',
            values: [{
              columns: [
                {
                  aggregated: { index: [''], metrics: { sum_m1: 5, sum_m2: 6 } },
                  key: '',
                  values: [{ index: [''], metrics: { sum_m1: 5, sum_m2: 6 } }],
                },
              ],
              index: ['a2', 'b2'],
            }],
          }],
      },
    ];

    it('handles multiple groups', () => {
      const groupby = ['a', 'b'];
      const metrics = [{
        d3format: null,
        description: null,
        expression: 'SUM(m1)',
        id: 1,
        metric_name: 'sum_m1',
        verbose_name: null,
        waring_text: null,
      }, {
        d3format: null,
        description: null,
        expression: 'SUM(m2)',
        id: 1,
        metric_name: 'sum_m2',
        verbose_name: null,
        waring_text: null,
      }];

      const nester = createRowNester(groupby, metrics, []);

      const nested = nester(sourceRows);

      expect(nested).toEqual(nestedRows);
    });

    it('can aggregate', () => {
      const metrics = [{
        d3format: null,
        description: null,
        expression: 'SUM(m1)',
        id: 1,
        metric_name: 'sum_m1',
        verbose_name: null,
        waring_text: null,
      }, {
        d3format: null,
        description: null,
        expression: 'SUM(m2)',
        id: 1,
        metric_name: 'sum_m2',
        verbose_name: null,
        waring_text: null,
      }];

      const aggFn = aggregateRows(metrics, []);

      const aggregated = nestedRows.map(aggregateNested(aggFn));

      expect(aggregated).toEqual(aggregatedRows);

    });
  });
});
