#!/bin/bash
set -ex

if [ "$#" -ne 0 ]; then
    exec "$@"
else
    gunicorn --bind  0.0.0.0:8088 \
        --workers $((2 * $(getconf _NPROCESSORS_ONLN) + 1)) \
        --timeout 60 \
        --limit-request-line 0 \
        --limit-request-field_size 0 \
        superset:app
fi
