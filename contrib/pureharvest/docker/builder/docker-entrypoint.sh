#!/bin/bash
set -ex

if [ "$#" -ne 0 ]; then
    exec "$@"
elif [ "$SUPERSET_ENV" = "development" ]; then
    flask run -p 8088 --with-threads --reload --debugger --host=0.0.0.0
else
    superset --help
fi
